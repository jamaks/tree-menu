export function debounce(fn: any, wait: number) {
  let t: any;
  return function () {
    clearTimeout(t);
    //@ts-ignore
    t = setTimeout(() => fn.apply(this, arguments), wait);
  };
}
