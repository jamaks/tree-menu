import React, { useState } from 'react';
import './App.css';
import { TreeMenu } from './tree-menu/tree-menu';
import { SearchInput } from './search-input/search-input';

function App() {
    const [searchText, setSearch] = useState('');

    const url = window.location.pathname.slice(1);

    function urlChanged(url: string) {
        window.history.pushState(url, document.title, url);
    }

    return (
        <nav className="app-sidebar">
            <SearchInput onChange={(val: any) => setSearch(val)}></SearchInput>
            <TreeMenu searchText={searchText} url={url} urlChanged={urlChanged}></TreeMenu>
        </nav>
    );
}

export default App;
