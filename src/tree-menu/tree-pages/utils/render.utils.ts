/**
 * Gets class item to page tree
 */
export function getItemClassByProps(page: any, props: any) {
    const isSelected = page.url !== undefined && props.activeUrl === page.url;
    const itemClass = 'tree-menu-pages-item' + (isSelected ? ' tree-menu-pages-item__selected' : '');

    return itemClass;
}

export function hasPageChilds(page: any) {
    return Array.isArray(page.anchors) || Array.isArray(page.pages);
}
