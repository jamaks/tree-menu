/**
 * Search id by url in pages
 */
export function searchIdByUrl(pages: any, url: string): string | undefined {
    for (let id of Object.keys(pages)) {
        if (pages[id].url === url) {
            return id
        }
    }
    return undefined
}

/**
 * Gets actualize opened ids
 */
export function getActualOpenedIdsById(openedIds: Set<any>, pages: any, id: any) {
    openedIds.add(id)
    let parentId = pages[id]?.parentId

    while (parentId) {
        openedIds.add(parentId)
        parentId = pages[parentId]?.parentId
    }

    return openedIds
}

/**
 * Search text on pages and add parents
 */
export function searchPages(allPages: any, searchText: string) {
    const foundPages: any = {}

    searchInNodes(allPages, searchText, (id: string) => {
        foundPages[id] = allPages[id]
        addParentPages(allPages[id]?.parentId, foundPages, allPages)
    })

    return foundPages
}

/**
 * Search anchors and add parents pages
 */
export function searchAnchors(anchors: any, searchText: string, allPages: any, foundPages: any) {
    const foundAnchors: any = {}

    searchInNodes(anchors, searchText, (id: string) => {
        foundAnchors[id] = anchors[id]
        addParentPages(anchors[id]?.parentId, foundPages, allPages)
    })

    return foundAnchors
}

/**
 * Search text in nodes and call foundPredicate if exist
 */
function searchInNodes(nodes: any, searchText: string, foundPredicate: any) {
    for (let id of Object.keys(nodes)) {
        if (nodes[id]?.title?.toLowerCase().includes(searchText)) {
            foundPredicate(id)
        }
    }
}

/**
 * Add all parents in page
 */
function addParentPages(parentId: string, foundPages: any, allPages: any) {
    while (parentId) {
        foundPages[parentId] = allPages[parentId]
        parentId = foundPages[parentId]?.parentId
    }
}
