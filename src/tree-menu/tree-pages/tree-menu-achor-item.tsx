import React from 'react';
import { TREE_PADDING_SIZE } from './consts/render.const';
import { getItemClassByProps } from './utils/render.utils';

export class TreeMenuAnchorItem extends React.PureComponent<any, any> {
    onAnchorClick = (e: any) => {
        e.preventDefault();
        this.props.onAnchorClick(this.props.id);
    };

    render() {
        const props: any = this.props;
        const anchor = props.anchors[props.id];
        const page = props.page;

        if (!page || !anchor) return <></>;

        const padding = TREE_PADDING_SIZE * 2 + TREE_PADDING_SIZE * page.level;
        const itemClass = getItemClassByProps(page, props);

        return (
            <li onClick={this.onAnchorClick} className={itemClass} style={{ paddingLeft: padding + 'px' }}>
                <a className="tree-menu-pages-item--link" href={anchor.url + anchor.anchor}>
                    {anchor.title}
                </a>
            </li>
        );
    }
}
