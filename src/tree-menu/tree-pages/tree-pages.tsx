import React from 'react';
import './tree-pages.css';
import { TreeMenuPageItem } from './tree-menu-page-item';
import { getActualOpenedIdsById, searchAnchors, searchIdByUrl, searchPages } from './utils/search-tree.utils';

export class TreePages extends React.Component<{ toc: any; urlChanged: any; url: string; searchText: string }, any> {
    searchText: any;

    constructor(props: any) {
        super(props);

        this.state = {
            openedIds: new Set(),
            activeId: undefined,
            activeUrl: '',
        };
    }

    /**
     * Actualize nodes by seearch text
     */
    componentDidUpdate() {
        const searchText = this.props.searchText.toLowerCase();

        if (this.searchText === searchText || !this.props.toc?.entities.pages) {
            return;
        }

        this.searchText = searchText;

        if (!searchText) {
            this.setState({
                foundPages: undefined,
                foundAnchors: undefined,
            });

            return;
        }

        const foundPages: any = searchPages(this.props.toc.entities.pages, searchText);

        const foundAnchors: any = searchAnchors(
            this.props.toc.entities.anchors,
            searchText,
            this.props.toc.entities.pages,
            foundPages
        );

        // Close opened items on search
        const openedIds: Set<any> = this.state.openedIds;
        openedIds.clear();

        this.setState({
            foundPages,
            foundAnchors,
            openedIds,
            activeUrl: '',
        });
    }

    /**
     * Init component
     */
    componentDidMount() {
        const url = this.props.url;
        const pages = this.props.toc.entities.pages;

        const activeId = searchIdByUrl(pages, this.props.url);
        const openedIds: Set<any> = getActualOpenedIdsById(this.state.openedIds, pages, activeId);

        this.setState({
            activeUrl: url,
            activeId,
            openedIds,
        });
    }

    onPageClick(id: any) {
        const pages = this.props.toc.entities.pages;
        const page = pages[id];
        const openedIds: Set<any> = this.state.openedIds;

        if (openedIds.has(id)) {
            openedIds.delete(id);
            this.closeChildsPages(page.pages, pages);
        } else {
            openedIds.add(id);
        }

        this.setState({ openedIds, activeUrl: page.url });

        this.props.urlChanged(page.url);
    }

    public onAnchorClick(id: any) {
        const pages = this.props.toc.entities.pages;
        const anchors = this.props.toc.entities.anchors;

        const anchor = anchors[id];
        const page = pages[anchor.parentId];

        const openedIds: Set<any> = this.state.openedIds;
        openedIds.add(page.id);
        this.setState({ openedIds, activeUrl: page.url });

        this.props.urlChanged(page.url + anchor.anchor);
    }
    render() {
        const openedIds = this.state.openedIds;
        const activeUrl = this.state.activeUrl;

        const toc = this.props?.toc;
        const topLevelIds = toc?.topLevelIds;

        const pages = this.state?.foundPages || toc?.entities.pages;
        const anchors = this.state?.foundAnchors || toc?.entities.anchors;

        const hasItemsObject = Object.keys(pages || anchors).length > 0;

        if (hasItemsObject) {
            return (
                <ul className="tree-menu-pages">
                    {topLevelIds?.map((id: any) => (
                        <TreeMenuPageItem
                            key={id}
                            id={id}
                            pages={pages}
                            anchors={anchors}
                            activeUrl={activeUrl}
                            openedIds={openedIds}
                            onAnchorClick={(id: any) => this.onAnchorClick(id)}
                            onPageClick={(id: any) => this.onPageClick(id)}
                        />
                    ))}
                </ul>
            );
        } else {
            return <div className="not-found-container">Not found</div>;
        }
    }

    /**
     * Recursive remove page childs opened ids
     */
    private closeChildsPages(pageIds: any[], pages: any) {
        if (pageIds) {
            const openedIds: Set<any> = this.state.openedIds;

            pageIds.forEach((id) => {
                openedIds.delete(id);
                this.closeChildsPages(pages[id].pages, pages);
            });
        }
    }
}
