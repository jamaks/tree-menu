import React from 'react';
import { TreeMenuAnchorItem } from './tree-menu-achor-item';
import { TREE_PADDING_SIZE } from './consts/render.const';
import { getItemClassByProps, hasPageChilds } from './utils/render.utils';

const CLOSE_ANIMATION_TIME = 200;
const OPEN_ANIMATION_TIME = 50;
const INIT_OPEN_ANIMATION_TIME = 100;

const OPENED_CLASS = 'item-opened';
const CLOSED_CLASS = 'item-closed';
const COLAPSED_CLASS = 'item-colapsed';

const CHEVRON_CLASS = 'chevron-up';
const CHEVRON_CLASS_DOWN = 'chevron-up--down';

/**
 * Tree menu page component
 */
export class TreeMenuPageItem extends React.PureComponent<any, any> {
    updateAnimationTimeout: any;
    initAnimationTimeout: any;

    /**
     * Page click and run animation for close/open
     */
    onPageClick = (e: any) => {
        e.preventDefault();

        const props: any = this.props;
        const page = props.pages[props.id];
        const openedIds = props.openedIds;

        if (openedIds.has(page.id)) {
            // close animation
            this.setState({ collapsed: true, opened: false });
            this.initAnimationTimeout = setTimeout(() => {
                this.props.onPageClick(page.id);
            }, CLOSE_ANIMATION_TIME);
        } else {
            // open animation
            this.props.onPageClick(page.id);
            this.initAnimationTimeout = setTimeout(() => {
                this.setState({ collapsed: false, opened: true });
            }, OPEN_ANIMATION_TIME);
        }
    };

    onPageItemClick = (id: any) => {
        this.props.onPageClick(id);
    };

    onAnchorClick = (id: any) => {
        this.props.onAnchorClick(id);
    };

    /**
     * Animation in update for example on init page
     */
    componentDidUpdate() {
        const openedIds = this.props.openedIds;

        if (openedIds.has(this.props.id) && !this.state?.collapsed) {
            this.updateAnimationTimeout = setTimeout(() => {
                this.setState({ collapsed: false, opened: true });
            }, INIT_OPEN_ANIMATION_TIME);
        }
    }

    /**
     * Destroy component
     */
    componentWillUnmount() {
        clearTimeout(this.initAnimationTimeout);
        clearTimeout(this.updateAnimationTimeout);
    }

    render() {
        const props: any = this.props;
        const openedIds = props.openedIds;
        const page = props.pages[props.id];

        if (!page) return <></>;

        const hasChilds = hasPageChilds(page);
        const pageOpened = openedIds.has(props.id);

        let chevronClassName = CHEVRON_CLASS;
        let children;

        if (hasChilds && pageOpened) {
            chevronClassName += ' ' + CHEVRON_CLASS_DOWN;
            children = this.getChildsElement(page);
        }

        const url = page.url || '/';
        const padding = TREE_PADDING_SIZE + TREE_PADDING_SIZE * page.level;
        const itemClass = getItemClassByProps(page, props);

        return (
            <>
                <li onClick={this.onPageClick} className={itemClass} style={{ paddingLeft: padding + 'px' }}>
                    {hasChilds && <span className={chevronClassName}></span>}
                    <a href={url} className="tree-menu-pages-item--link">
                        {page.title}
                    </a>
                </li>
                {children}
            </>
        );
    }

    /**
     * Gets chils elements
     */
    getChildsElement(page: any) {
        // Animations class
        let pageGroupClassName = this.state?.opened ? OPENED_CLASS : CLOSED_CLASS;
        pageGroupClassName += this.state?.collapsed ? ' ' + COLAPSED_CLASS : '';

        const pages = this.props.pages;
        const anchors = this.props.anchors;
        const activeUrl = this.props.activeUrl;
        const openedIds = this.props.openedIds;

        return (
            <div className={pageGroupClassName}>
                {page.anchors?.map((el: any) => (
                    <TreeMenuAnchorItem
                        key={el}
                        id={el}
                        activeUrl={activeUrl}
                        anchors={anchors}
                        page={page}
                        onAnchorClick={(id: any) => this.onAnchorClick(id)}
                    />
                ))}
                {page.pages?.map((el: any) => (
                    <TreeMenuPageItem
                        key={el}
                        id={el}
                        activeUrl={activeUrl}
                        anchors={anchors}
                        pages={pages}
                        openedIds={openedIds}
                        onPageClick={(id: any) => this.onPageItemClick(id)}
                        onAnchorClick={(id: any) => this.onAnchorClick(id)}
                    />
                ))}
            </div>
        );
    }
}
