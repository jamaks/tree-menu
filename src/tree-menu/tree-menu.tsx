import React from 'react';
import { TreeLoader } from './tree-loader/tree-loader';
import { TreePages } from './tree-pages/tree-pages';
import { getTreeData } from './tree-menu-resource';

export class TreeMenu extends React.Component<
    { url: string; urlChanged: any; searchText: string },
    { toc: any; loading: boolean }
> {
    constructor(props: any) {
        super(props);
        this.state = { toc: undefined, loading: true };
    }

    componentDidMount() {
        getTreeData().then((result) => {
            this.setState({
                toc: result,
                loading: false,
            });
        });
    }
    render() {
        return (
            <div className="tree-menu">
                {this.state.loading ? (
                    <TreeLoader></TreeLoader>
                ) : (
                    <TreePages
                        toc={this.state?.toc}
                        url={this.props.url}
                        urlChanged={this.props.urlChanged}
                        searchText={this.props.searchText}
                    ></TreePages>
                )}
            </div>
        );
    }
}
