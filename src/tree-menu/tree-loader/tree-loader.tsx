import React from 'react'
import './tree-loader.css'

const VIEW_ELEMENTS_COUNT = 8

export class TreeLoader extends React.PureComponent {
    public items = Array.apply(null, Array(VIEW_ELEMENTS_COUNT)).map((_, i) => (
        <li key={i} className="tree-loader__item"></li>
    ))

    render() {
        return <ul className="tree-loader">{this.items}</ul>
    }
}
