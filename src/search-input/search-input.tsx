import React from 'react';
import { debounce } from '../utils/time.utils';
import './search-input.css';

export class SearchInput extends React.Component<any, any> {
    changeTextDebounce: any;

    constructor(props: any) {
        super(props);

        this.changeTextDebounce = debounce(this.changeText.bind(this), 500);
    }

    changeText(event: any) {
        this.props.onChange(event.target.value);
    }

    render() {
        return (
            <form className="search-container">
                <input
                    className="search-container-input"
                    placeholder="Search..."
                    autoComplete="off"
                    type="text"
                    name="name"
                    onChange={this.changeTextDebounce}
                />
            </form>
        );
    }
}
